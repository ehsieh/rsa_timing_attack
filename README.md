# RSA Timing Attack Framework #

The RSA cryptosystem is an industry standard encryption algorithm that is widely used to secure private data. While mathematically sound, the RSA algorithm is potentially vulnerable to side channel attacks. One such attack is evident through the time it takes to encrypt messages based on key value. The goal of this project is to create an FPGA framework to launch timing attacks on RSA implementations. This allows the designer to evaluate the security of his or her RSA implementation. Currently, the attack method is to measure the cycle count and compare it with the cycle count of the actual key.

### Requirements ###
* MATLAB: http://www.mathworks.com/products/matlab/?requestedDomain=www.mathworks.com
* Xilinx Vivado (2015.4 or later): http://www.xilinx.com/products/design-tools/vivado.html 
* Silabs UART Driver: https://www.silabs.com/products/mcu/Pages/USBtoUARTBridgeVCPDrivers.aspx

### Supported FPGA Platforms ###
* Xilinx VC707
* Xilinx KC705

### Data Flow ###
1. MATLAB sends the key to the attack core to measure the cycle count need to encrypt each plaintext (supplied by a psuedo random number generator -- LFSR)
2. Core returns the count to MATLAB to calculate the variance (we call this org_var).
3. MATLAB then attempts to guess the key, one bit at a time; by observing the variance in the cycle count (test_var). 
4. The bit (1 or 0) results the least difference (org_var - test_var) is chosen.
5. Repeat with each bit.

### System Diagram ###

This framework allows the user to drop in any RSA Core, synthesize on a FPGA and run.

The Attack Core consists of the following:

* UART Core - To handle communication between the FPGA and MATLAB application.
* Control Module - To synchronize the RSA Core, UART, and FIFOs.
* RSA Core - Actual Implementation of the RSA Core to attack.
* Counter - To count the number of cycles needed to attack the core.
* Pseudo Random Number Generator - Generate Plaintext for encryption.

![Untitled Diagram.png](https://bitbucket.org/repo/ErkKK5/images/3955375790-Untitled%20Diagram.png)

### Sample Data ###
To be Added

### Team Members ###
* Vinnie Hu
* Chen Yang
* Eric Hsieh
* Xiaoxi Liu