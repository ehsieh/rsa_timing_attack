%Guess 0 is input 1, Guess 1 is input 2
function output = voting_algo(input_1, input_2, plain_text)
    index_end = length(plain_text); %Gets the # of plain_text
    bin = 20;                       %How many groups we want to break it into
    step = index_end / bin;         %400
    start = 1;
    limit = step;
    for n = 1:bin
%         output_1(n) = var(input_1(start:limit));    %Variance of Guess 0
%         output_2(n) = var(input_2(start:limit));    %Variance of Guess 1
%         key_output = var(plain_text(start:limit));  %Variance of Key
        
        %Variance(Key - Guess0)
        output_1(n) = var( (plain_text(start:limit) - input_1(start:limit)));
        
        %Variance(Key - Guess1)
        output_2(n) = var( (plain_text(start:limit) - input_2(start:limit)));
        
        temp(n) = limit;           %For Graphing
        start = start + step;
        limit = limit + step;
    end
%    output_1 = key_output' - output_1';     %
%    output_2 = key_output' - output_2';
    
    count = 0;
    for x= 1 : length(output_1)
        diff = output_2(x) - output_1(x);
        if(diff < 0)
            %If Variance of Guess 1 < Guess 0, then vote for 1
            count = count + 1;
        end
    end
    
    %If Guess 1 has > 50% of Vote
    if(count >= (bin/2))
        output = 1;
    else
        output = 0;
    end
end