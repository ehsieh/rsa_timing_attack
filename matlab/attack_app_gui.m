function varargout = attack_app_gui(varargin)
% attack_APP_GUI MATLAB code for attack_app_gui.fig
%      attack_APP_GUI, by itself, creates a new attack_APP_GUI or raises the existing
%      singleton*.
%
%      H = attack_APP_GUI returns the handle to a new attack_APP_GUI or the handle to
%      the existing singleton*.
%
%      attack_APP_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in attack_APP_GUI.M with the given input arguments.
%
%      attack_APP_GUI('Property','Value',...) creates a new ATTAC_APP_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before attac_app_gui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to attac_app_gui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help attac_app_gui

% Last Modified by GUIDE v2.5 09-Apr-2016 13:25:17

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @attac_app_gui_OpeningFcn, ...
                   'gui_OutputFcn',  @attac_app_gui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before attac_app_gui is made visible.
function attac_app_gui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to attac_app_gui (see VARARGIN)

% Choose default command line output for attac_app_gui
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes attac_app_gui wait for user response (see UIRESUME)
% uiwait(handles.figure1);

set(handles.com_port_string, 'string', 'COM4');


% --- Outputs from this function are returned to the command line.
function varargout = attac_app_gui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function plaintext_file_path_Callback(hObject, eventdata, handles)
% hObject    handle to plaintext_file_path (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of plaintext_file_path as text
%        str2double(get(hObject,'String')) returns contents of plaintext_file_path as a double


% --- Executes during object creation, after setting all properties.
function plaintext_file_path_CreateFcn(hObject, eventdata, handles)
% hObject    handle to plaintext_file_path (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function private_key_Callback(hObject, eventdata, handles)
% hObject    handle to private_key (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of private_key as text
%        str2double(get(hObject,'String')) returns contents of private_key as a double


% --- Executes during object creation, after setting all properties.
function private_key_CreateFcn(hObject, eventdata, handles)
% hObject    handle to private_key (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function public_key_Callback(hObject, eventdata, handles)
% hObject    handle to public_key (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of public_key as text
%        str2double(get(hObject,'String')) returns contents of public_key as a double


% --- Executes during object creation, after setting all properties.
function public_key_CreateFcn(hObject, eventdata, handles)
% hObject    handle to public_key (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function com_port_string_Callback(hObject, eventdata, handles)
% hObject    handle to com_port_string (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of com_port_string as text
%        str2double(get(hObject,'String')) returns contents of com_port_string as a double

% --- Executes during object creation, after setting all properties.
function com_port_string_CreateFcn(hObject, eventdata, handles)
% hObject    handle to com_port_string (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in attack_button.
function attack_button_Callback(hObject, eventdata, handles)
% hObject    handle to attack_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


%TODO:
%Open PlainText File
%disp('Opening File...')
%plaintext_filename = get(handles.plaintext_file_path, 'String');
%plaintext_fileID = fopen(plaintext_filename, 'r');
%format_spec = '%u';


%Read it into a vector
%plaintext_vector = fscanf(plaintext_fileID, format_spec);
%fclose(plaintext_fileID);

%disp(plaintext_vector);

%start command list

IDLE = uint8(hex2dec('00'));
RECEIVE = uint8(hex2dec('05'));
KEY = uint8(hex2dec('55'));
MOD = uint8(hex2dec('5A'));
SEED = uint8(hex2dec('A5'));
START = uint8(hex2dec('AA'));

%Open coms with the FPGA
fprintf('\nStart...\n')
com_port_string = get(handles.com_port_string, 'String');
port = serial(com_port_string);
set(port, 'BaudRate', 115200);
set(port, 'InputBufferSize', 8192);

fopen(port);
fprintf('Opening %s, BaudRate = %d ...\n', port.Port, port.BaudRate);

%Send Commands
fwrite(port, uint8(hex2dec('00'))); %Set into IDLE
fwrite(port, uint8(hex2dec('05'))); %Set into Receive State
key = hex2dec({'00';'90';'3A';'D9'});
fwrite(port, key, 'uint8');
fwrite(port, uint8(hex2dec('55'))); % Send Key
fwrite(port, uint8(hex2dec('05'))); % Set into Receive
data = hex2dec({'03';'B2';'C1';'59'});
fwrite(port, data, 'uint8');
fwrite(port, MOD); % Send Mod
fwrite(port, RECEIVE); % Set into Receive
data = hex2dec({'00';'72';'41';'83'});
fwrite(port, data, 'uint8');
fwrite(port, SEED); % Set Seed
fwrite(port, START); % START

% The floowing command also tested and worked
% cmd = hex2dec({'00';'05';'00';'90';'3A';'D9';'55';'05';'03';'B2';'C1';'59';'5A';'05';'00';'72';'41';'83';'A5';'AA'});
% fwrite(port, cmd, 'uint8');

%Initial Key Cycles
raw_key_cycles = fread(port, 8000); % Read 8000 bytes off the serial port
fclose(port);
fprintf('%s is Closed.\n', port.Port);


%------- Debug only -------
%data_dump_file_name = 'matlab_data_dump.txt';
%output_file = fopen(data_dump_file_name, 'wt+');
%fprintf(output_file, '%d\n', data);
%fclose(output_file);


%[s, d] = size(data);
%fprintf( '%d bytes data has been saved to %s\n', s, data_dump_file_name);
%fprintf( 'matlab_data_dump.txt\n' );
%------- Debug only -------

%Start Attempt on Bit = 1
fopen(port);

fwrite(port, IDLE); %Set into IDLE
fwrite(port, RECEIVE); %Set into Receive State
key = hex2dec({'00';'00';'00';'39'});
fwrite(port, key, 'uint8');
fwrite(port, KEY); % Send Key
fwrite(port, RECEIVE); % Set into Receive
data = hex2dec({'03';'B2';'C1';'59'});
fwrite(port, data, 'uint8');
fwrite(port, MOD); % Send Mod
fwrite(port, RECEIVE); % Set into Receive
data = hex2dec({'00';'72';'41';'83'});
fwrite(port, data, 'uint8');
fwrite(port, SEED); % Set Seed
fwrite(port, START); % START
raw_guess_1_cycles = fread(port, 8000);

%Start Attempt on Bit = 0
fwrite(port, IDLE); %Set into IDLE
fwrite(port, RECEIVE); %Set into Receive State
key = hex2dec({'00';'00';'00';'19'});
fwrite(port, key, 'uint8');
fwrite(port, KEY); % Send Key
fwrite(port, RECEIVE); % Set into Receive
data = hex2dec({'03';'B2';'C1';'59'});
fwrite(port, data, 'uint8');
fwrite(port, MOD); % Send Mod
fwrite(port, RECEIVE); % Set into Receive
data = hex2dec({'00';'72';'41';'83'});
fwrite(port, data, 'uint8');
fwrite(port, SEED); % Set Seed
fwrite(port, START); % START
raw_guess_0_cycles = fread(port, 8000);

fclose(port);

cycle_length = length(raw_key_cycles)/2;
key_cycles = zeros(1, cycle_length);
for n = 1:cycle_length
  key_cycles(n) = raw_key_cycles(2*n-1) * 256 + raw_key_cycles(2*n);
end

guess_0_cycles = zeros(1, cycle_length);
for n = 1:cycle_length
  guess_0_cycles(n) = raw_guess_0_cycles(2*n-1) * 256 + raw_guess_0_cycles(2*n);
end

guess_1_cycles = zeros(1, cycle_length);
for n = 1:cycle_length
  guess_1_cycles(n) = raw_guess_1_cycles(2*n-1) * 256 + raw_guess_1_cycles(2*n);
end

diff_key_1 = key_cycles - guess_1_cycles;
diff_key_0 = key_cycles - guess_0_cycles;

guess_1_var = var(diff_key_1);
guess_0_var = var(diff_key_0);

%Send First element with Key
%fprintf(s, data);

%Wait for FPGA READY Signal
%fscanf(s, ready);
%
%
%

%Store clock cycles
%fscanf(s, cycles);


% disp('Attacking...')
% plaintext_filename = get(handles.plaintext_file_path, 'String');
% private_key = get(handles.private_key, 'String');
% public_key = get(handles.public_key, 'String');
% 
% disp(strcat('plain_text_file = ', plaintext_filename));
% disp(strcat('private key = ', private_key));
% disp(strcat('public key = ', public_key));

% ----------------- Debug only --------------
file_name = 'key_runtime_dump.txt';
output_file = fopen(file_name, 'wt+');
fprintf(output_file, '%d\n', key_cycles);
fclose(output_file);

file_name = 'key0_runtime_dump.txt';
output_file = fopen(file_name, 'wt+');
fprintf(output_file, '%d\n', guess_0_cycles);
fclose(output_file);

file_name = 'key1_runtime_dump.txt';
output_file = fopen(file_name, 'wt+');
fprintf(output_file, '%d\n', guess_1_cycles);
fclose(output_file);

% -------------------------------------------
fprintf('Guess 1 Variance = %f\n', guess_1_var);
fprintf('Guess 0 Variance = %f\n', guess_0_var);
disp('Done!')


