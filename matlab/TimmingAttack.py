import serial
import array
import numpy as np
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
from math import sqrt

BATCH_SIZE = 4000

cmd_reset = [0x00]
cmd_data  = [0x05]
cmd_key   = [0x55]
cmd_mod   = [0x5A]
cmd_seed  = [0xA5]
cmd_test  = [0xAA]

test_key 	= [0x00, 0x90, 0x3A, 0xD9]
modulus 	= [0x03, 0xB2, 0xC1, 0x59]
rand_seed = [0x00, 0x72, 0x41, 0x83]	

#key0 = [0x00, 0x00, 0x3A, 0xD9]
#key1 = [0x00, 0x01, 0x3A, 0xD9]
key0 = [0x00, 0x00, 0x00, 0x19]
key1 = [0x00, 0x00, 0x00, 0x59]
#test_cmd = [0x00, 0x05, 0x00, 0x90, 0x3A, 0xD9, 0x55, 0x05, 0x03, 0xB2, 0xC1, 0x59, 0x5A, 0x05, 0x00, 0x72, 0x41, 0x83, 0xA5, 0xAA]

np.set_printoptions(precision=2, threshold=200, linewidth=80)
np.set_printoptions(formatter={'int':lambda x:hex(int(x))})
ser = serial.Serial(3, 115200, timeout=1)  # open serial port
print(ser.portstr + " is opened")          # check which port was really used

def draw_distribution_plot(data, title):
	mu = np.mean(data)
	sigma = sqrt(np.var(data))
	n, bins, patches = plt.hist(data, 50, normed=1, facecolor='green', alpha=0.75)

	# add a 'best fit' line
	y = mlab.normpdf( bins, mu, sigma)
	l = plt.plot(bins, y, 'r--', linewidth=1)

	plt.xlabel('Runtime distribution of ' + title)
	plt.ylabel('Probability')
	plt.grid(True)
	plt.show()	
	
def serial_send(data):
	data = np.array(data)
	#print("\nSending Command ==>\n", data)
	cmd = array.array('B', data).tostring()
	ser.write(cmd)

def bytes_to_int(bytes):
	#print("\n<== ", bytes)
	rtn = []
	hexStr = bytes.hex()
	for i in range(0, len(hexStr), 2):
		rtn.append(int (hexStr[i:i+2], 16 ))
	return np.array(rtn)

def convert_bytes_to_runtime(bytes):
	data = bytes_to_int( bytes )
	#print("\nRaw Data:\n", data)
	rtn = []
	for i in range(0, data.size, 2):
		rtn += [data[i]*256 + data[i+1]]
	rtn = np.array(rtn)
	#print("\nRuntime:\n", rtn[:100])
	return rtn
	
def test(key):
	print("Test with key: ", np.array(key))
	cmd = []
	cmd += cmd_reset
	cmd += cmd_data + rand_seed + cmd_seed 	# update random seed
	cmd += cmd_data + modulus + cmd_mod 		# updat modulus
	cmd += cmd_data + key + cmd_key 				# update key
	cmd += cmd_test													# start test
	serial_send(cmd)

	line = ser.read(2*BATCH_SIZE)
	runtime = convert_bytes_to_runtime(line)
	print("Runtime:\n", runtime[:200])
	print("Mean=", np.mean(runtime))
	print("Var=", np.var(runtime))
	print("\n")
	return runtime

def guess_bit(T0, T1):
	var0 = np.var(T0)
	print("Var(T-T0) = %f" % var0)
	print("Mean = ", np.mean(T0))
	
	var1 = np.var(T1)
	print("Var(T-T1) = %f" % var1)
	print("Mean = ", np.mean(T1))
	
	rtn = 0
	if(var1 > var0):
		rtn = 1
	print("Guessing bit is ", rtn)
	return rtn

# ====== Strat test ======
print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
T = test(test_key)
T0 = test(key0)
T1 = test(key1)

T_T0 = T - T0
T_T1 = T - T1
bit = guess_bit(T_T0, T_T1)

filename = "log_files/T.txt"
np.savetxt(filename, T, fmt="%d") 
draw_distribution_plot(T, 'T')

filename = "log_files/T_T0.txt"
np.savetxt(filename, T0, fmt="%d") 
draw_distribution_plot(T, 'T0')

filename = "log_files/T_T1.txt"
np.savetxt(filename, T1, fmt="%d") 
draw_distribution_plot(T, 'T1')



# print("\n=================================================================================")
# key1 = [0x00, 0x00, 0x00, 0x59]
# T1 = T - test(key1)
# bit = guess_bit(T0, T1)

# print("\n=================================================================================")
# T0 = T1
# key1 = [0x00, 0x00, 0x00, 0xD9]
# T1 = T - test(key1)
# bit = guess_bit(T0, T1)

# ====== End =======
ser.close()             # close port
print("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")
print(ser.portstr + " is Closed...")
print("Done!\n")

