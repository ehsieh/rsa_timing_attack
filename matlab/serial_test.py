import serial
import array
import numpy as np

np.set_printoptions(precision=2, threshold=10000, linewidth=80)
np.set_printoptions(formatter={'int':lambda x:hex(int(x))})

def bytes_to_int(bytes):
	rtn = []
	hexStr = bytes.hex()
	for i in range(0, len(hexStr), 2):
		rtn.append(int (hexStr[i:i+2], 16 ))
	return np.array(rtn)
	
test_cmd = [0x00, 0x05, 0x00, 0x90, 0x3A, 0xD9, 0x55, 0x05, 0x03, 0xB2, 0xC1, 0x59, 0x5A, 0x05, 0x00, 0x72, 0x41, 0x83, 0xA5, 0xAA]
test_cmd = np.array(test_cmd)
cmd = array.array('B', test_cmd).tostring()
print("\narray with binary format = ", cmd)

ser = serial.Serial(3, 115200, timeout=1)  # open serial port
print(ser.portstr)       # check which port was really used
ser.write(cmd)
#line = ser.readline()
line = ser.read(8000)
ser.close()             # close port

print("\nRead Line = ", line)
runtime = bytes_to_int( line )
#print("\nRuntime = ", runtime)
print("\nSize = ", runtime.size)
