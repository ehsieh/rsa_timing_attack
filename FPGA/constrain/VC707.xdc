# clk pins
set_property PACKAGE_PIN E19 [get_ports clk_p]
set_property IOSTANDARD LVDS [get_ports clk_p]
set_property PACKAGE_PIN E18 [get_ports clk_n]
set_property IOSTANDARD LVDS [get_ports clk_n]
# reset pin
set_property PACKAGE_PIN AV40 [get_ports rst_pin]
set_property IOSTANDARD LVCMOS18 [get_ports rst_pin]
# uart pins
set_property PACKAGE_PIN AU36 [get_ports txd_pin]
set_property IOSTANDARD LVCMOS18 [get_ports txd_pin]
#set_property PACKAGE_PIN AT32 [get_ports USB_UART_RTS]
#set_property IOSTANDARD LVCMOS18 [get_ports USB_UART_RTS]
set_property PACKAGE_PIN AU33 [get_ports rxd_pin]
set_property IOSTANDARD LVCMOS18 [get_ports rxd_pin]
#set_property PACKAGE_PIN AR34 [get_ports USB_UART_CTS]
#set_property IOSTANDARD LVCMOS18 [get_ports USB_UART_CTS]
# led pins
set_property PACKAGE_PIN AM39 [get_ports led_pins[0]]
set_property IOSTANDARD LVCMOS18 [get_ports led_pins[0]]
set_property PACKAGE_PIN AN39 [get_ports led_pins[1]]
set_property IOSTANDARD LVCMOS18 [get_ports led_pins[1]]
set_property PACKAGE_PIN AR37 [get_ports led_pins[2]]
set_property IOSTANDARD LVCMOS18 [get_ports led_pins[2]]
set_property PACKAGE_PIN AT37 [get_ports led_pins[3]]
set_property IOSTANDARD LVCMOS18 [get_ports led_pins[3]]
set_property PACKAGE_PIN AR35 [get_ports done_led]
set_property IOSTANDARD LVCMOS18 [get_ports done_led]
#set_property PACKAGE_PIN AP41 [get_ports led_pins[5]]
#set_property IOSTANDARD LVCMOS18 [get_ports led_pins[5]]
#set_property PACKAGE_PIN AP42 [get_ports led_pins[6]]
#set_property IOSTANDARD LVCMOS18 [get_ports led_pins[6]]
#set_property PACKAGE_PIN AU39 [get_ports led_pins[7]]
#set_property IOSTANDARD LVCMOS18 [get_ports led_pins[7]]