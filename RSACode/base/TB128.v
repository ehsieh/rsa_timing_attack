// this is a testbench file for the GLIFT logic of RSA

`timescale 1ns/1ns

module TB128();
  reg clk, rst, ds;
  reg [127:0] key, mod;
  
  wire rdy;
  wire [127:0] message, cipher;
  
  reg [11:0] count;
  
  RSACypher RSA(.indata(message),
			.inExp(key),
			.inMod(mod),
			.cypher(cipher),
			.clk(clk),
			.ds(ds),
			.reset(rst),
			.ready(rdy));

  lfsr  lfsr128(ds, rst, message);
  
  // reset conditions
  initial
  begin
    clk <= 0;
    rst <= 1;
    ds <= 0;
    
    key <= 128'b0;
    mod <= 128'b0;
    
    #200
    rst <= 0;
    
    #20
    key <= 128'h41f3e9076cbc5e9435b1451635b81619;
    mod <= 128'hc091042aace654a3bca20dcd4346ee81;
    
    #20
    ds <= 1;
    
    #10
    ds <= 0;    
  end
  
  // generating a clock signal here
  always
      #5  clk <= ~clk;
      
  always @ (posedge clk) begin
      if(rdy == 1 && cipher != 128'h0) begin     
        #10
        ds <= 1;
    
        #10
        ds <= 0;
      end
  end

  always @ (posedge clk or posedge rst) begin
      if(rst)
		count <= 0;
	 else begin
		if(ds)
		  if(count == 1) $stop;
			else count <= count + 1;
	 end
  end

endmodule
