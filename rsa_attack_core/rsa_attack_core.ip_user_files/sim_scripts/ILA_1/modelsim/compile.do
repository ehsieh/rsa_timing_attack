vlib work
vlib msim

vlib msim/xil_defaultlib

vmap xil_defaultlib msim/xil_defaultlib

vlog -work xil_defaultlib -64 -incr "+incdir+../../../../rsa_attack_core.srcs/sources_1/ip/ILA_1/ila_v6_0_1/hdl/verilog" "+incdir+../../../../rsa_attack_core.srcs/sources_1/ip/ILA_1/ltlib_v1_0_0/hdl/verilog" "+incdir+../../../../rsa_attack_core.srcs/sources_1/ip/ILA_1/xsdbm_v1_1_1/hdl/verilog" "+incdir+../../../../rsa_attack_core.srcs/sources_1/ip/ILA_1/xsdbs_v1_0_2/hdl/verilog" "+incdir+../../../../rsa_attack_core.srcs/sources_1/ip/ILA_1/ila_v6_0_1/hdl/verilog" "+incdir+../../../../rsa_attack_core.srcs/sources_1/ip/ILA_1/ltlib_v1_0_0/hdl/verilog" "+incdir+../../../../rsa_attack_core.srcs/sources_1/ip/ILA_1/xsdbm_v1_1_1/hdl/verilog" "+incdir+../../../../rsa_attack_core.srcs/sources_1/ip/ILA_1/xsdbs_v1_0_2/hdl/verilog" \
"../../../../rsa_attack_core.srcs/sources_1/ip/ILA_1/sim/ILA_1.v" \


vlog -work xil_defaultlib "glbl.v"

