`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04/12/2016 06:28:09 PM
// Design Name: 
// Module Name: rsa_attack_core
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module rsa_attack_core(input clk_n,
                       input clk_p,
                       input rst_pin,
                       input rxd_pin,
                       input USB_UART_CTS,
                       output USB_UART_RTS,
                       output txd_pin);

    wire clk;
    
    //UART lines
    //wire r_xmitH;
    //wire [7:0] r_tx_buffer;
    reg [7:0] r_tx_buffer;
    reg r_xmitH;
    wire r_xmit_doneH;
    wire [7:0] r_rx_buffer;
    wire r_receive_done;
    
    //Control lines
    wire ready;
    wire [127:0] private_key;
    wire [127:0] public_key;
    reg [127:0] msg = 128'hDEADBEEF;
    
    //RSA Core Lines
    wire [127:0] cipher;
    wire rsa_done;
    wire ds;
    
    //Coutner Lines
    wire [31:0] cycle_count;
                        
    //UART TEST LINES
    reg rx_data_done;
    reg [7:0] counter;
    reg [7:0] index_rx;
    reg [7:0] rx_data [0:4];
    reg rx_data_done;
    
    reg rx_ready_to_transmit;
    reg rx_ready_for_transmit;
                        
    clk_wiz_1 clk_gen( .clk_in1_p(clk_p),
                       .clk_in1_n(clk_n),
                       .clk_out1(clk),
                       .reset(rst_pin));
                      
    uart uart_core(.clk(clk),
                  .rst_h(rst_pin),
                                  
                  .uart_XMIT_dataH(txd_pin),
                  .xmitH(r_xmitH),
                  .xmit_dataH(r_tx_buffer),
                  .xmit_doneH(r_xmit_doneH),
                                  
                   // Receiver
                  .uart_REC_dataH(rxd_pin),
                  .rec_dataH(r_rx_buffer),
                  .rec_readyH(r_receive_done));

                     
    always @(posedge clk)
    begin
        if(rst_pin)
        begin
            rx_data_done <= 0;
        end
        if(r_receive_done)
        begin
            if(index_rx < 4)
            begin
                rx_data[index_rx] <= r_rx_buffer;
                index_rx <= index_rx + 1;
                rx_data_done <= 0;
            end
        end
        if(index_rx >= 4)
        begin
            index_rx <= 5;
            rx_data[4] <= 8'h0A;
            rx_data_done <= 1;
        end
    end
   
   always @(posedge clk, posedge rst_pin)
   begin
        if(rst_pin)
          counter <= 0;
        else begin           
        if(rx_data_done)
        begin
          if(r_xmitH == 0 && rx_ready_to_transmit == 1)
          begin
              if( counter < 5)
              begin
                  r_tx_buffer <= rx_data[counter];
                  counter <= counter + 1;
              end
              else
              begin
                  r_tx_buffer <= 8'h00;
              end
              
              r_xmitH = 1;
          end
          else if(r_xmitH == 0 && counter == 0)
          begin
              r_tx_buffer <= rx_data[counter];
              counter <= counter + 1;
              r_xmitH = 1;
          end
          else
          begin
              r_xmitH = 0;
          end
        end
        end
        end

    always @(posedge clk)
    begin
      if(rst_pin)
      begin
          rx_ready_to_transmit <= 0;
          rx_ready_for_transmit <= 0;
          
          
      end 
      else if (r_xmit_doneH && !rx_ready_for_transmit && counter < 5 && rx_data_done)
      begin
          rx_ready_to_transmit <= 1;
      end
      else
      begin
          rx_ready_to_transmit <= 0;
      end    
      
      rx_ready_for_transmit <= r_xmit_doneH;   
    end
    
    
endmodule
