`timescale 1ns/1ns

module rsa_attack_core_tb();

reg clk; 
wire t_uart_clk;
reg reset;

//transmitter lines
wire t_tx_data;
reg t_xmitH;
reg [7:0] t_tx_buffer;
wire t_xmit_doneH;

wire t_rx_data;
wire [7:0] t_rx_buffer;
wire t_recieve_done;

//receiver lines
wire r_tx_data;
wire r_xmitH;
wire [7:0] r_tx_buffer;
wire r_xmit_doneH;

wire r_rx_data;
wire [7:0] r_rx_buffer;
wire r_receive_done;

wire [127:0] private_key;
wire [127:0] public_key;
wire ready;

reg [127:0] msg = 128'hDEADBEEF;
wire [127:0] cipher;
wire rsa_done;
wire ds;

wire [31:0] cycle_count;

uart uart_core_transmitter(.clk(clk),
               .rst_h(reset),
                               
               .uart_XMIT_dataH(t_tx_data),
               .xmitH(t_xmitH),
               .xmit_dataH(t_tx_buffer),
               .xmit_doneH(t_xmit_doneH),
                               
                // Receiver
               .uart_REC_dataH(r_tx_data),
               .rec_dataH(t_rx_buffer),
               .rec_readyH(t_recieve_done));


uart uart_core_receiver(.clk(clk),
               .rst_h(reset),
                               
               .uart_XMIT_dataH(r_tx_data),
               .xmitH(r_xmitH),
               .xmit_dataH(r_tx_buffer),
               .xmit_doneH(r_xmit_doneH),
                               
                // Receiver
               .uart_REC_dataH(t_tx_data),
               .rec_dataH(r_rx_buffer),
               .rec_readyH(r_receive_done));

control_module control( .i_clk(clk),
                        .i_reset(reset),
                        .i_r_xmit_doneH(r_xmit_doneH),
                        .i_uart_rx_done(r_receive_done),
                        .i_uart_rx_data(r_rx_buffer),
                        .i_cycle_count(cycle_count),
                        .i_rsa_done(rsa_done),
                        
                        .o_private_key(private_key),
                        .o_public_key(public_key),
                        .o_ready(ready),
                        .o_ds(ds),
                        .o_r_xmitH(r_xmitH),
                        .o_r_tx_data(r_tx_buffer));  

counter count( .i_ready(ready),
               .i_clk(clk),
               .i_stop(rsa_done),
               .i_reset(reset),
    
               .o_count(cycle_count));


RSACypher RSA(.indata(msg),
              .inExp(private_key),
              .inMod(public_key),
              .cypher(cipher),
              .clk(clk),
              .ds(ds),
              .reset(reset),
              .ready(rsa_done));

baud_rate test_baud(.clk(clk),
                    .rst_h(reset),
                    
                    .baud_clk(t_uart_clk));

reg ready_to_transmit;
reg ready_for_transmit;
reg [7:0] index;
reg [7:0] priv_key[0:3];
reg [7:0] mod_key[0:3];
reg [7:0] pub_key[0:3];

reg [31:0] key_32;
reg [31:0] mod_32; 


initial begin
    clk = 0;
    reset = 1;
    t_xmitH = 0;
    ready_to_transmit = 0;
    
    index = 8'd0;
    
    //key <= 128'b0;
    //mod <= 128'b0;
    
    mod_32 <= 32'ha10a9545;
    key_32 <= 32'h26ab19ad;
    
    priv_key[0] = 8'h45;
    priv_key[1] = 8'h95;
    priv_key[2] = 8'h0a;
    priv_key[3] = 8'ha1;
    
    mod_key[0] = 8'had;
    mod_key[1] = 8'h19;
    mod_key[2] = 8'hab;
    mod_key[3] = 8'h26;
    

    #10 reset = 0;    
end

always @(posedge clk)
begin
    if(reset)
    begin
        t_xmitH <= 0;
        t_tx_buffer <= 8'h00;
        index = 0;
    end
    else
    begin
        if(t_xmitH == 0 && ready_to_transmit == 1)
        begin
            if(index < 4)
            begin
                t_tx_buffer <= priv_key[index];
                index = index + 1;
                t_xmitH = 1;
            end
            else if(index >= 4 && index < 8)
            begin
                t_tx_buffer <= mod_key[index - 4];
                index = index + 1;
                t_xmitH = 1;
            end
            else
            begin
                t_tx_buffer <= 8'h00;
                t_xmitH = 0;
                //index = 0;
            end
            
              
        end
        else
        begin
            t_xmitH = 0;
        end
    end
end

always @(posedge clk)
begin
    if(reset)
    begin
        ready_to_transmit <= 0;
        ready_for_transmit <= 0;
    end
    else if (t_xmit_doneH == 1 && ready_for_transmit == 0)
    begin
        ready_to_transmit <= 1;
    end
    else
    begin
        ready_to_transmit <= 0;
    end    
    
    ready_for_transmit <= t_xmit_doneH;  
    
end

//always
//begin
//    @(posedge clk)
//    begin
        
//    end
//end

always
begin
    #5 clk = ~clk;
end

//configure receiver logic
/*
always
begin 

    r_tx_buffer = r_rx_buffer;
    if(r_recieve_done)
    begin
        r_xmitH = 1;
    end
    else
    begin
        r_xmitH = 0;
    end
end
*/


endmodule

module baud_rate(clk,
            rst_h,
               
            baud_clk);

  // The xtal-osc clock freq
  parameter XTAL_CLK = 100000000;

  // The desired baud rate
  parameter BAUD = 256000;
  parameter CLK_DIV = XTAL_CLK / (BAUD * 16 * 2);

  // CW >= log2(CLK_DIV)
  parameter CW = 5;

  input clk;
  input rst_h;
  output baud_clk;

  reg [CW-1:0] clk_div;
  reg baud_clk;

  always @(posedge clk or posedge rst_h)
    if (rst_h) begin
      clk_div  <= 0;
      baud_clk <= 0;
    end
    else if (clk_div == CLK_DIV) begin
      clk_div  <= 0;
      baud_clk <= ~baud_clk;
 	  end
    else begin
      clk_div  <= clk_div + 1;
      baud_clk <= baud_clk;
    end

endmodule