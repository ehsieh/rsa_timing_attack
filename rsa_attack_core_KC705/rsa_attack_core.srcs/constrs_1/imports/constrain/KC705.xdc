#CLOCKS
#SYSCLK
set_property PACKAGE_PIN AD11 [get_ports clk_n]
set_property IOSTANDARD LVDS [get_ports clk_n]
set_property PACKAGE_PIN AD12 [get_ports clk_p]
set_property IOSTANDARD LVDS [get_ports clk_p]
#USERCLK
#set_property PACKAGE_PIN K29 [get_ports clk_n]
#set_property IOSTANDARD LVDS_25 [get_ports clk_n]
#set_property PACKAGE_PIN K28 [get_ports clk_p]
#set_property IOSTANDARD LVDS_25 [get_ports clk_p]
#GPIO PUSHBUTTON SW
set_property PACKAGE_PIN AB7 [get_ports rst_pin]
set_property IOSTANDARD LVCMOS15 [get_ports rst_pin]
#USB UART
set_property PACKAGE_PIN L27 [get_ports USB_UART_RTS]
set_property IOSTANDARD LVCMOS25 [get_ports USB_UART_RTS]
set_property PACKAGE_PIN K23 [get_ports USB_UART_CTS]
set_property IOSTANDARD LVCMOS25 [get_ports USB_UART_CTS]
set_property PACKAGE_PIN K24 [get_ports txd_pin]
set_property IOSTANDARD LVCMOS25 [get_ports txd_pin]
set_property PACKAGE_PIN M19 [get_ports rxd_pin]
set_property IOSTANDARD LVCMOS25 [get_ports rxd_pin]