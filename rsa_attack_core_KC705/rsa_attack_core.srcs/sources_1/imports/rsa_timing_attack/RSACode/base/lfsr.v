// this is 128-bit long LFSR

module lfsr(clk, reset, lfsr);
  input clk, reset;
  output reg [127:0] lfsr;
  wire d0;

  xnor(d0, lfsr[127], lfsr[125], lfsr[100], lfsr[98]);

  always @(posedge clk, posedge reset) begin
    if(reset) begin
      lfsr <= 128'he2d2b66aaef4ea33;
    end
    else begin
      lfsr <= {lfsr[126:0], d0};
    end
  end
endmodule
