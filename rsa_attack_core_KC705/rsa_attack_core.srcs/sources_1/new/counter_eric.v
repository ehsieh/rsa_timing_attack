//-----------------------------------------------------
// Design Name : counter
// File Name   : counter.v
// Function    : Up counter
// Coder       : Eric Hsieh
//-----------------------------------------------------

module counter	(
i_ready				,	// start count
i_clk				,	// clock Input
i_stop				,   // stop count
i_reset				,	// reset Input
o_count					// Output of the counter
);

//----------Output Ports--------------
output [31:0] o_count;

//------------Input Ports--------------
input i_ready, i_clk, i_reset, i_stop;

//------------Internal Variables--------
reg [31:0] o_count;
reg enable;

//-------------Code Starts Here-------
always @(posedge i_clk)
if (i_reset) begin
  o_count <= 32'b0 ;
end else if (i_ready == 0 && i_stop == 0) begin
  o_count <= o_count + 1;
end

endmodule 
