`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04/12/2016 06:28:09 PM
// Design Name: 
// Module Name: rsa_attack_core
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module rsa_attack_core(input clk_n,
                       input clk_p,
                       input rst_pin,
                       input rxd_pin,
                       input USB_UART_CTS,
                       output USB_UART_RTS,
                       output txd_pin);

    reg [7:0] rx_data [0:4];
    reg [7:0] index;
    reg [7:0] counter;
    
    reg rx_ready_to_transmit;
    reg rx_ready_for_transmit;

    reg r_xmitH;
    reg [7:0] r_tx_buffer;
    wire r_xmit_doneH;

    wire [7:0] r_rx_buffer;
    wire r_receive_done;

                        
    clk_wiz_kc705 clk_gen( .clk_in1_p(clk_p),
                       .clk_in1_n(clk_n),
                       .clk_out1(clk_2_uart),
                       .reset(rst_pin));
                      
    uart uart_core(.clk(clk_2_uart),
                   .rst_h(rst_pin),
                                   
                   .uart_XMIT_dataH(txd_pin),
                   .xmitH(r_xmitH),
                   .xmit_dataH(r_tx_buffer),
                   .xmit_doneH(r_xmit_doneH),
                                   
                   // Receiver
                   .uart_REC_dataH(rxd_pin),
                   .rec_dataH(r_rx_buffer),
                   .rec_readyH(r_receive_done));
    
    always @(*)
    begin
        rx_data[0] = 8'h54;
        rx_data[1] = 8'h45;
        rx_data[2] = 8'h53;
        rx_data[3] = 8'h54;
        rx_data[4] = 8'h0A;
    end
                   
    always@(posedge clk_2_uart)
    begin
        if(rst_pin)
        begin
            counter <= 0;
            r_xmitH <=0;
            rx_ready_to_transmit <= 0;
            rx_ready_for_transmit <= 0;
        end
       
        if(r_xmitH == 0 && rx_ready_to_transmit == 1)
        begin
            if( counter < 5)
            begin
                r_tx_buffer <= rx_data[counter];
                counter <= counter + 1;
            end
            else
            begin
                r_tx_buffer <= 8'h0A;
                counter <= 0;
            end
          
            r_xmitH = 1;
        end
        else if(r_xmitH == 0 && counter == 0)
        begin
            r_tx_buffer <= rx_data[counter];
            counter <= counter + 1;
            r_xmitH = 1;
        end
        else
        begin
            r_xmitH = 0;
        end 
               
        if (r_xmit_doneH && !rx_ready_for_transmit)
        begin
            rx_ready_to_transmit <= 1;
        end
        else
        begin
            rx_ready_to_transmit <= 0;
        end    
            
        rx_ready_for_transmit <= r_xmit_doneH;
    end
    
    
    
endmodule
