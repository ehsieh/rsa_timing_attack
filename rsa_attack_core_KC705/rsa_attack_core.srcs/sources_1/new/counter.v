`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/02/2016 10:12:38 PM
// Design Name: 
// Module Name: counter
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module counter(
    input i_ready,
    input i_clk,
    input i_stop,
    input i_reset,
    
    output reg [31:0] o_count
    );

reg [31:0] count;
always @(posedge i_clk)
begin
    if(i_reset)
    begin
        o_count <= 32'b0;
    end
    else
    begin
        if(i_ready == 1 && i_stop == 1)
        begin
            o_count <= 32'b0;
        end
        
        if(i_stop == 0 && i_ready == 0)
        begin
            o_count <= o_count + 1;
        end
        
    end
end

endmodule
