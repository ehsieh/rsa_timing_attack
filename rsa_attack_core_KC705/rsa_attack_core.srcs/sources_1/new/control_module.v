//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04/26/2016 11:50:55 PM
// Design Name: 
// Module Name: control_module
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module control_module( i_clk,
                       i_reset,
                       i_r_xmit_doneH,
                       i_uart_rx_done,
                       i_uart_rx_data,
                       i_cycle_count,
                       i_rsa_done,
                       
                       o_private_key,
                       o_public_key,
                       o_ready,
                       o_ds,
                       o_r_xmitH,
                       o_r_tx_data);

parameter BITS = 16'd32; 

input i_clk;
input i_reset;
input i_uart_rx_done;
input [7:0] i_uart_rx_data;
input [31:0] i_cycle_count;
input i_rsa_done;

output reg [127:0] o_private_key;
output reg [127:0] o_public_key;
output reg o_ready;
output reg o_ds;

//from Uart
input i_r_xmit_doneH;

//to Uart
output reg o_r_xmitH;
output reg [7:0] o_r_tx_data;

reg [7:0] cycles[4:0];
reg ready_for_transmit = 1;
reg ready_to_transmit = 0;

reg [31:0] count = 0;
reg [7:0] index = 0;

always @(posedge i_clk)
begin
    if(i_reset)
    begin
        o_ready <= 0;
        o_private_key <= 128'b0;
        o_public_key <= 128'b0;
        o_ds <= 0;
        o_r_tx_data <= 0;
        
    end
end

always @(posedge i_uart_rx_done)
begin
    if(i_reset)
    begin
        o_private_key <= 128'b0;
        o_public_key <= 128'b0;
        o_ready <= 0;
        o_ds <= 0;
        count <= 0;
    end
    else
    begin
        case(count)
            0: 
            begin
                o_private_key[7:0]  = 128'b0 | i_uart_rx_data;
                count <= count + 1;
                o_ready <= 0;
            end
            1: 
            begin
                o_private_key[15:8]  = 128'b0 | i_uart_rx_data;
                count <= count + 1;
                o_ready <= 0;
            end
            2: 
            begin
                o_private_key[23:16]  = 128'b0 | i_uart_rx_data;
                count <= count + 1;
                o_ready <= 0;
            end
            3: 
            begin
                o_private_key[31:24]  = 128'b0 | i_uart_rx_data;
                count <= count + 1;
                o_ready <= 0;
            end 
            4: 
            begin
                o_public_key[7:0]  = 128'b0 | i_uart_rx_data;
                count <= count + 1;
                o_ready <= 0;
            end 
            5: 
            begin
                o_public_key[15:8]  = 128'b0 | i_uart_rx_data;
                count <= count + 1;
                o_ready <= 0;
            end
            6: 
            begin
                o_public_key[23:16]  = 128'b0 | i_uart_rx_data;
                count <= count + 1;
                o_ready <= 0;
            end
            7: 
            begin
                o_public_key[31:23]  = 128'b0 | i_uart_rx_data;
                count <= count + 1;
                o_ready <= 0;
            end          
            default:
            begin
                count <= 0;
            end
        endcase
    end
end

//Prep the TX

always @(posedge i_rsa_done)
begin
    cycles[0] <= i_cycle_count[7:0];
    cycles[1] <= i_cycle_count[15:8];
    cycles[2] <= i_cycle_count[23:16];
    cycles[3] <= i_cycle_count[31:24];
    cycles[4] <= 8'h0A; //Line Feed for matlab
end

always @(posedge i_clk)
begin
    if(i_reset)
    begin
        o_r_xmitH <= 0;
        o_r_tx_data <= 8'h00;
        index <= 0;
    end
    else
    begin
        if(o_r_xmitH == 0 && ready_to_transmit == 1)
        begin
            if(index < 5)
            begin
                o_r_tx_data <= cycles[index];
                index <= index + 1;
            end
            else
            begin
                o_r_tx_data <= 8'h00;
                index <= 0;
            end
            
            o_r_xmitH <= 1;  
        end
        else
        begin
            o_r_xmitH <= 0;
        end
    end
end

always @(posedge i_clk)
begin
    if(i_reset)
    begin
        ready_to_transmit <= 0;
        ready_for_transmit <= 0;
    end
    else if (i_r_xmit_doneH == 1 && ready_for_transmit == 0)
    begin
        ready_to_transmit <= 1;
    end
    else
    begin
        ready_to_transmit <= 0;
    end    
    
    ready_for_transmit <= i_r_xmit_doneH;  
    

    
end

//DS Start up and down

//always @(negedge o_ready)
//begin
//    o_ds <= 1;
//end

always @(posedge i_clk)
begin
    if(count == 8)
    begin
        o_ds <= 1;
        count <= 0;
    end
    else
    begin
        o_ds <= 0;
    end
end



endmodule
