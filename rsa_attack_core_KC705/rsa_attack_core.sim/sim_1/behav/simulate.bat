@echo off
set xv_path=C:\\Xilinx\\Vivado\\2015.4\\bin
call %xv_path%/xsim rsa_attack_core_tb_behav -key {Behavioral:sim_1:Functional:rsa_attack_core_tb} -tclbatch rsa_attack_core_tb.tcl -log simulate.log
if "%errorlevel%"=="0" goto SUCCESS
if "%errorlevel%"=="1" goto END
:END
exit 1
:SUCCESS
exit 0
