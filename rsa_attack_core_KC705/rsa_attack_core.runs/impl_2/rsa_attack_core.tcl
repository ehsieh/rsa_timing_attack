proc start_step { step } {
  set stopFile ".stop.rst"
  if {[file isfile .stop.rst]} {
    puts ""
    puts "*** Halting run - EA reset detected ***"
    puts ""
    puts ""
    return -code error
  }
  set beginFile ".$step.begin.rst"
  set platform "$::tcl_platform(platform)"
  set user "$::tcl_platform(user)"
  set pid [pid]
  set host ""
  if { [string equal $platform unix] } {
    if { [info exist ::env(HOSTNAME)] } {
      set host $::env(HOSTNAME)
    }
  } else {
    if { [info exist ::env(COMPUTERNAME)] } {
      set host $::env(COMPUTERNAME)
    }
  }
  set ch [open $beginFile w]
  puts $ch "<?xml version=\"1.0\"?>"
  puts $ch "<ProcessHandle Version=\"1\" Minor=\"0\">"
  puts $ch "    <Process Command=\".planAhead.\" Owner=\"$user\" Host=\"$host\" Pid=\"$pid\">"
  puts $ch "    </Process>"
  puts $ch "</ProcessHandle>"
  close $ch
}

proc end_step { step } {
  set endFile ".$step.end.rst"
  set ch [open $endFile w]
  close $ch
}

proc step_failed { step } {
  set endFile ".$step.error.rst"
  set ch [open $endFile w]
  close $ch
}

set_msg_config -id {HDL 9-1061} -limit 100000
set_msg_config -id {HDL 9-1654} -limit 100000

start_step init_design
set rc [catch {
  create_msg_db init_design.pb
  set_param xicom.use_bs_reader 1
  create_project -in_memory -part xc7k325tffg900-2
  set_property board_part xilinx.com:kc705:part0:1.2 [current_project]
  set_property design_mode GateLvl [current_fileset]
  set_property webtalk.parent_dir S:/git_repository/RSA_Timing_Attack/rsa_attack_core_KC705/rsa_attack_core.cache/wt [current_project]
  set_property parent.project_path S:/git_repository/RSA_Timing_Attack/rsa_attack_core_KC705/rsa_attack_core.xpr [current_project]
  set_property ip_repo_paths {
  s:/git_repository/RSA_Timing_Attack/rsa_attack_core_KC705/rsa_attack_core.cache/ip
  S:/git_repository/RSA_Timing_Attack/rsa_attack_core_KC705/rsa_attack_core.srcs/sources_1/ip
} [current_project]
  set_property ip_output_repo s:/git_repository/RSA_Timing_Attack/rsa_attack_core_KC705/rsa_attack_core.cache/ip [current_project]
  add_files -quiet S:/git_repository/RSA_Timing_Attack/rsa_attack_core_KC705/rsa_attack_core.runs/synth_1/rsa_attack_core.dcp
  add_files -quiet S:/git_repository/RSA_Timing_Attack/rsa_attack_core_KC705/rsa_attack_core.runs/clk_wiz_kc705_synth_1/clk_wiz_kc705.dcp
  set_property netlist_only true [get_files S:/git_repository/RSA_Timing_Attack/rsa_attack_core_KC705/rsa_attack_core.runs/clk_wiz_kc705_synth_1/clk_wiz_kc705.dcp]
  read_xdc -mode out_of_context -ref clk_wiz_kc705 -cells inst s:/git_repository/RSA_Timing_Attack/rsa_attack_core_KC705/rsa_attack_core.srcs/sources_1/ip/clk_wiz_kc705/clk_wiz_kc705_ooc.xdc
  set_property processing_order EARLY [get_files s:/git_repository/RSA_Timing_Attack/rsa_attack_core_KC705/rsa_attack_core.srcs/sources_1/ip/clk_wiz_kc705/clk_wiz_kc705_ooc.xdc]
  read_xdc -prop_thru_buffers -ref clk_wiz_kc705 -cells inst s:/git_repository/RSA_Timing_Attack/rsa_attack_core_KC705/rsa_attack_core.srcs/sources_1/ip/clk_wiz_kc705/clk_wiz_kc705_board.xdc
  set_property processing_order EARLY [get_files s:/git_repository/RSA_Timing_Attack/rsa_attack_core_KC705/rsa_attack_core.srcs/sources_1/ip/clk_wiz_kc705/clk_wiz_kc705_board.xdc]
  read_xdc -ref clk_wiz_kc705 -cells inst s:/git_repository/RSA_Timing_Attack/rsa_attack_core_KC705/rsa_attack_core.srcs/sources_1/ip/clk_wiz_kc705/clk_wiz_kc705.xdc
  set_property processing_order EARLY [get_files s:/git_repository/RSA_Timing_Attack/rsa_attack_core_KC705/rsa_attack_core.srcs/sources_1/ip/clk_wiz_kc705/clk_wiz_kc705.xdc]
  read_xdc S:/git_repository/RSA_Timing_Attack/rsa_attack_core_KC705/rsa_attack_core.srcs/constrs_1/imports/constrain/KC705.xdc
  link_design -top rsa_attack_core -part xc7k325tffg900-2
  close_msg_db -file init_design.pb
} RESULT]
if {$rc} {
  step_failed init_design
  return -code error $RESULT
} else {
  end_step init_design
}

start_step place_design
set rc [catch {
  create_msg_db place_design.pb
  catch {write_hwdef -file rsa_attack_core.hwdef}
  place_design 
  write_checkpoint -force rsa_attack_core_placed.dcp
  report_io -file rsa_attack_core_io_placed.rpt
  report_utilization -file rsa_attack_core_utilization_placed.rpt -pb rsa_attack_core_utilization_placed.pb
  report_control_sets -verbose -file rsa_attack_core_control_sets_placed.rpt
  close_msg_db -file place_design.pb
} RESULT]
if {$rc} {
  step_failed place_design
  return -code error $RESULT
} else {
  end_step place_design
}

start_step route_design
set rc [catch {
  create_msg_db route_design.pb
  route_design 
  write_checkpoint -force rsa_attack_core_routed.dcp
  report_drc -file rsa_attack_core_drc_routed.rpt -pb rsa_attack_core_drc_routed.pb
  report_timing_summary -warn_on_violation -max_paths 10 -file rsa_attack_core_timing_summary_routed.rpt -rpx rsa_attack_core_timing_summary_routed.rpx
  report_power -file rsa_attack_core_power_routed.rpt -pb rsa_attack_core_power_summary_routed.pb
  report_route_status -file rsa_attack_core_route_status.rpt -pb rsa_attack_core_route_status.pb
  report_clock_utilization -file rsa_attack_core_clock_utilization_routed.rpt
  close_msg_db -file route_design.pb
} RESULT]
if {$rc} {
  step_failed route_design
  return -code error $RESULT
} else {
  end_step route_design
}

start_step write_bitstream
set rc [catch {
  create_msg_db write_bitstream.pb
  catch { write_mem_info -force rsa_attack_core.mmi }
  write_bitstream -force rsa_attack_core.bit 
  catch { write_sysdef -hwdef rsa_attack_core.hwdef -bitfile rsa_attack_core.bit -meminfo rsa_attack_core.mmi -file rsa_attack_core.sysdef }
  close_msg_db -file write_bitstream.pb
} RESULT]
if {$rc} {
  step_failed write_bitstream
  return -code error $RESULT
} else {
  end_step write_bitstream
}

